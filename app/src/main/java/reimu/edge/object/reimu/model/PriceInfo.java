package reimu.edge.object.reimu.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by owner on 3/14/18.
 */

public class PriceInfo implements Parcelable{

    protected PriceInfo(Parcel in) {
        amount = in.readString();
    }

    public static final Creator<PriceInfo> CREATOR = new Creator<PriceInfo>() {
        @Override
        public PriceInfo createFromParcel(Parcel in) {
            return new PriceInfo(in);
        }

        @Override
        public PriceInfo[] newArray(int size) {
            return new PriceInfo[size];
        }
    };

    public String getAmount() {
        return amount;
    }

    public void setAmount(String priceInfo) {
        this.amount = priceInfo;
    }

    private String amount;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(amount);
    }
}
