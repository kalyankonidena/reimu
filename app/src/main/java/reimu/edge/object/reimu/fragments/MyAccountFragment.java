package reimu.edge.object.reimu.fragments;

import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import reimu.edge.object.reimu.R;
import reimu.edge.object.reimu.adapters.MyAccountAdapter;
import reimu.edge.object.reimu.asynctasks.LoginAsyncTask;
import reimu.edge.object.reimu.asynctasks.OrderAsyncTask;
import reimu.edge.object.reimu.asynctasks.OrderDetailsAsyncTask;
import reimu.edge.object.reimu.model.OrderDetailsModel;

public class MyAccountFragment extends Fragment implements OrderAsyncTask.IOrdersCallBack, OrderDetailsAsyncTask.IOrdersDetailsCallBack,LoginAsyncTask.ILoginCallBack {

    private ArrayList<OrderDetailsModel> mOrderDetailsModelList;
    private ListView mOrdersListView;
    private JSONArray mOrders;
    private TextView mSubscriptionAmount;
    private ProgressDialog mProgressDialog;
    private View mRootView;

    public static MyAccountFragment newInstance() {
        MyAccountFragment fragment = new MyAccountFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.fragment_my_account, container, false);
        mProgressDialog = ProgressDialog.show(getActivity(), "", "Please Wait...", true);
        mOrdersListView = (ListView) mRootView.findViewById(R.id.my_account_subscriptions_list);
        mSubscriptionAmount = (TextView) mRootView.findViewById(R.id.subscriptionTotalAmount);
        mRootView.setVisibility(View.INVISIBLE);
        mOrderDetailsModelList = new ArrayList<>();

        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {

        LoginAsyncTask loginAsyncTask = new LoginAsyncTask(this);
        loginAsyncTask.execute();

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onOrdersDataCallBack(JSONArray orders) {

        mOrders = orders;
        for (int i = 0; i < orders.length(); i++) {
            try {
                OrderDetailsAsyncTask orderDetailsAsyncTask = new OrderDetailsAsyncTask(this);
                orderDetailsAsyncTask.execute(new JSONObject(orders.getString(i)).getString("id"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    public void onOrdersDetailsDataCallBack(OrderDetailsModel orderDetails) {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
        mRootView.setVisibility(View.VISIBLE);

        mOrderDetailsModelList.add(orderDetails);
        if ((mOrders != null && mOrderDetailsModelList != null) && (mOrders.length() == mOrderDetailsModelList.size())) {
            MyAccountAdapter myAccountAdapter = new MyAccountAdapter(getLayoutInflater(), mOrderDetailsModelList);
            mOrdersListView.setAdapter(myAccountAdapter);
            calculateSubsciptionAmount(retrieveSubscriptionAmount(mOrderDetailsModelList));
        }
    }

    private float retrieveSubscriptionAmount(ArrayList<OrderDetailsModel> orderDetailsModelList) {
        float subscriptionTotal = 0.0f;
        int i = 0;
        for (OrderDetailsModel orderDetailsModel : orderDetailsModelList) {
            subscriptionTotal += Float.valueOf(orderDetailsModel.getCommerceItems()[i].getPriceInfo().getAmount());
        }
        return subscriptionTotal;
    }

    private void calculateSubsciptionAmount(float amount) {
        String subAmount = (String) mSubscriptionAmount.getText();
        subAmount = subAmount.replace("34.98", String.valueOf(amount));
        mSubscriptionAmount.setText(subAmount);
    }


    @Override
    public void onLoginCallBack(String loginResponse) {
        OrderAsyncTask orderAsyncTask = new OrderAsyncTask(this);
        orderAsyncTask.execute();
    }
}
