package reimu.edge.object.reimu.fragments;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.PopupWindow;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;

import reimu.edge.object.reimu.R;


public class ReimuFragment extends Fragment {


    public static final String YOUTUBE_API_KEY = "AIzaSyBb7okZ2kpGvCqCbHCOz0Zwy-RasHhipbA";
    private MyPlaybackEventListener playbackEventListener;
    private MyPlayerStateChangeListener playerStateChangeListener;


    private boolean isAddDisplayed = false;
    private static final int RECOVERY_REQUEST = 1;
    private YouTubePlayerView youTubeView;
    private YouTubePlayer mPlayer;
    private Handler handler;

    public ReimuFragment() {
        // Required empty public constructor
    }


    public static ReimuFragment newInstance() {
        ReimuFragment fragment = new ReimuFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_reimu, container, false);
        playbackEventListener = new MyPlaybackEventListener();
        playerStateChangeListener = new MyPlayerStateChangeListener();
        handler = new Handler();
        YouTubePlayerSupportFragment youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.youtube_layout, youTubePlayerFragment).commit();
        setRetainInstance(true);
        youTubePlayerFragment.initialize(YOUTUBE_API_KEY, new YouTubePlayer.OnInitializedListener() {

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider arg0, YouTubeInitializationResult youTubeInitializationResult) {
                if (youTubeInitializationResult.isUserRecoverableError()) {
                    youTubeInitializationResult.getErrorDialog(getActivity(), RECOVERY_REQUEST).show();
                }
            }

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {

                if (!wasRestored) {

                    mPlayer = player;
                    mPlayer.setPlayerStateChangeListener(playerStateChangeListener);
                    mPlayer.setPlaybackEventListener(playbackEventListener);

                    mPlayer.cueVideo("dXQiE-f9RuM");
                }
            }

        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
       if( mPlayer!=null && !mPlayer.isPlaying() ){
           mPlayer.play();
       }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void showAdd(String url) {
        final WebView wv = new WebView(getActivity());
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setDomStorageEnabled(true);
        wv.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        wv.getSettings().setMediaPlaybackRequiresUserGesture(false);


        wv.getSettings().setPluginState(WebSettings.PluginState.ON);
        wv.setWebChromeClient(new WebChromeClient());

        wv.loadUrl(url);


        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                //  wv.loadUrl("javascript:(function() { document.getElementsByClassName('ytp-large-play-button ytp-button')[0].click(); })()");

            }

            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                handler.proceed(); // Ignore SSL certificate errors
            }

            @Override
            public void onLoadResource(WebView view, String url) {

                super.onLoadResource(view, url);
            }
        });

        PopupWindow pw = new PopupWindow(wv, getActivity().getWindow().getWindowManager().getDefaultDisplay().getWidth(), getActivity().getWindow().getWindowManager().getDefaultDisplay().getHeight(), true);
        // display the popup in the center
        pw.showAtLocation(wv, Gravity.CENTER, 0, 0);

    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {

            int currentMilliseonds = 0;
            if (mPlayer != null)
                currentMilliseonds = mPlayer.getCurrentTimeMillis();

            if (currentMilliseonds >= 1000 && currentMilliseonds <= 10000) {
                mPlayer.pause();
                showAdd("file:///android_asset/add_video1.html");
            } else if (currentMilliseonds >= 10001 && currentMilliseonds <= 20000) {
                mPlayer.pause();
                showAdd("file:///android_asset/add_video2.html");

            }

        }

    };



    private final class MyPlaybackEventListener implements YouTubePlayer.PlaybackEventListener {

        @Override
        public void onPlaying() {
            Log.i("", "");

            if(!isAddDisplayed)
            handler.postDelayed(runnable, 10000);

        }

        @Override
        public void onPaused() {
            Log.i("", "");

        }

        @Override
        public void onStopped() {
            if(isAddDisplayed)
                isAddDisplayed = !isAddDisplayed;
            Log.i("", "");

        }

        @Override
        public void onBuffering(boolean b) {
            Log.i("", "");

        }

        @Override
        public void onSeekTo(int i) {
            Log.i("", "");

        }


    }

    private final class MyPlayerStateChangeListener implements YouTubePlayer.PlayerStateChangeListener {

        @Override
        public void onLoading() {
            Log.i("", "");

        }

        @Override
        public void onLoaded(String s) {
            Log.i("", s);
            // Called when a video is done loading.
            // Playback methods such as play(), pause() or seekToMillis(int) may be called after this callback.
        }



        @Override
        public void onAdStarted() {
            isAddDisplayed = true;
            Log.i("", "");
        }

        @Override
        public void onVideoStarted() {

        }

        @Override
        public void onVideoEnded() {

            Log.i("", "");
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason errorReason) {
            // Called when an error occurs.
        }
    }


}
