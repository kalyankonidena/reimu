package reimu.edge.object.reimu.asynctasks;

import android.os.AsyncTask;

import org.json.JSONArray;

import reimu.edge.object.reimu.fragments.MyAccountFragment;
import reimu.edge.object.reimu.network.GetOrdersAPI;
import reimu.edge.object.reimu.network.LoginApi;

/**
 * Created by owner on 3/19/18.
 */

public class LoginAsyncTask extends AsyncTask<Void,Void,String> {
    public interface ILoginCallBack {
        public void onLoginCallBack(String loginResponse);
    }

    private MyAccountFragment mAccountFragment;

    public LoginAsyncTask(MyAccountFragment accountFragment) {
        mAccountFragment = accountFragment;
    }

    @Override
    protected String doInBackground(Void... voids) {
        LoginApi loginApi = new LoginApi();
        String ordersData = loginApi.login();
        return ordersData;
    }

    @Override
    protected void onPostExecute(String loginResponse) {
        super.onPostExecute(loginResponse);
        mAccountFragment.onLoginCallBack(loginResponse);
    }

}
