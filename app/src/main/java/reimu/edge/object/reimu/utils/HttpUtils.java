package reimu.edge.object.reimu.utils;

import android.net.Uri;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by owner on 3/13/18.
 */

public class HttpUtils {

    private final static String CONTENT_TYPE = "Content-Type";
    private final static String APPLICATION_JSON = "application/json";
    private final static String NEW_LINE = "\n";


    public static String processRequestandGetResponse(URL weatherApiURL) throws IOException, JSONException{
        HttpResponse response = null;
        StringBuilder lineBuilder = new StringBuilder();

        String line = null;
        HttpClient httpClient = new DefaultHttpClient();
        HttpURLConnection connection =
                (HttpURLConnection) weatherApiURL.openConnection();
        HttpPost httpPost = new HttpPost(weatherApiURL.toString());
        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
        nameValuePair.add(new BasicNameValuePair("login", "johndoe@yopmail.com"));
        nameValuePair.add(new BasicNameValuePair("password", "password"));
        nameValuePair.add(new BasicNameValuePair("grant_type", "password"));


        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));

        } catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
        }

        try {
             response = httpClient.execute(httpPost);
            // write response to log
            Log.d("Http Post Response:", response.toString());
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            while ((line = reader.readLine()) != null) {
                lineBuilder.append(line);
            }
            reader.close();

        } catch (ClientProtocolException e) {
            // Log exception
            e.printStackTrace();
        } catch (IOException e) {
            // Log exception
            e.printStackTrace();
        }

        return lineBuilder.toString();
    }
}
