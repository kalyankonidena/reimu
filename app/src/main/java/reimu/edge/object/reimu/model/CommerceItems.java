package reimu.edge.object.reimu.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by owner on 3/14/18.
 */

public class CommerceItems implements Parcelable {

    private String catalogRefId;

    protected CommerceItems(Parcel in) {
        catalogRefId = in.readString();
        productId = in.readString();
        productDisplayName = in.readString();
        quantity = in.readString();
        priceInfo = in.readParcelable(PriceInfo.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(catalogRefId);
        dest.writeString(productId);
        dest.writeString(productDisplayName);
        dest.writeString(quantity);
        dest.writeParcelable(priceInfo, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CommerceItems> CREATOR = new Creator<CommerceItems>() {
        @Override
        public CommerceItems createFromParcel(Parcel in) {
            return new CommerceItems(in);
        }

        @Override
        public CommerceItems[] newArray(int size) {
            return new CommerceItems[size];
        }
    };

    public String getCatalogRefId() {
        return catalogRefId;
    }

    public void setCatalogRefId(String catalogRefId) {
        this.catalogRefId = catalogRefId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductDisplayName() {
        return productDisplayName;
    }

    public void setProductDisplayName(String productDisplayName) {
        this.productDisplayName = productDisplayName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public PriceInfo getPriceInfo() {
        return priceInfo;
    }

    public void setPriceInfo(PriceInfo priceInfo) {
        this.priceInfo = priceInfo;
    }

    private String productId;

    private String productDisplayName;

    private String quantity;

    private PriceInfo priceInfo;




}
