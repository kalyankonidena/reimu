package reimu.edge.object.reimu.network;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.net.URL;

import reimu.edge.object.reimu.model.OrderDetailsModel;
import reimu.edge.object.reimu.utils.HttpUtils;

/**
 * Created by owner on 3/13/18.
 */

public class GetOrderDetails {

    private static final String ORDER_DETAILS_API =
            "http://crs113.objectedge.com:7003/public/v1/orders/";
    private static final String TAG = GetOrdersAPI.class.getSimpleName();
    private String ordersDetailsData;

    public OrderDetailsModel getOrdersDetailsData(String orderNo) {
        OrderDetailsModel orderDetailsModel = null;
        try {
            ordersDetailsData = HttpUtils.processRequestandGetResponse(new URL(ORDER_DETAILS_API + orderNo));
        } catch (IOException ioException) {
            Log.e(TAG, ioException.getMessage());
        } catch (JSONException jsonException) {
            Log.e(TAG, jsonException.getMessage());
        }
        JSONArray jsonObject = null;
        try {
            Gson gson = new Gson();
            orderDetailsModel = gson.fromJson(ordersDetailsData, OrderDetailsModel.class);
            jsonObject = new JSONArray(ordersDetailsData);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return orderDetailsModel;

    }
}
