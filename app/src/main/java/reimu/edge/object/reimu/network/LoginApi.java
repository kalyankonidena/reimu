package reimu.edge.object.reimu.network;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.net.URL;

import reimu.edge.object.reimu.utils.HttpUtils;

/**
 * Created by owner on 3/19/18.
 */

public class LoginApi {

    private static final String LOGIN_API =
            "http://crs113.objectedge.com:7003/public/v1/currentUser/login";
    private static final String TAG = LoginApi.class.getSimpleName();
    private String orderDetails;

    public String login() {
        try {
            orderDetails = HttpUtils.processRequestandGetResponse(new URL(LOGIN_API));

        } catch (IOException ioException) {
            Log.e(TAG, ioException.getMessage());
        } catch (JSONException jsonException) {
            Log.e(TAG, jsonException.getMessage());

        }

        return orderDetails;
    }
}
