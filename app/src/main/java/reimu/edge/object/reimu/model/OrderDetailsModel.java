package reimu.edge.object.reimu.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by owner on 3/13/18.
 */

public class OrderDetailsModel implements Parcelable {

    private String id;

    protected OrderDetailsModel(Parcel in) {
        id = in.readString();
        commerceItems = in.createTypedArray(CommerceItems.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeTypedArray(commerceItems, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OrderDetailsModel> CREATOR = new Creator<OrderDetailsModel>() {
        @Override
        public OrderDetailsModel createFromParcel(Parcel in) {
            return new OrderDetailsModel(in);
        }

        @Override
        public OrderDetailsModel[] newArray(int size) {
            return new OrderDetailsModel[size];
        }
    };

    public CommerceItems[] getCommerceItems() {
        return commerceItems;
    }

    public void setCommerceItems(CommerceItems commerceItems[]) {
        this.commerceItems = commerceItems;
    }

    private CommerceItems commerceItems[];


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }






}
