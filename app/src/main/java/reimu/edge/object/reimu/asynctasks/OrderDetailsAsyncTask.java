package reimu.edge.object.reimu.asynctasks;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONObject;

import reimu.edge.object.reimu.fragments.MyAccountFragment;
import reimu.edge.object.reimu.model.OrderDetailsModel;
import reimu.edge.object.reimu.network.GetOrderDetails;
import reimu.edge.object.reimu.network.GetOrdersAPI;

/**
 * Created by owner on 3/13/18.
 */

public class OrderDetailsAsyncTask extends AsyncTask<String,Void,OrderDetailsModel> {

    public interface IOrdersDetailsCallBack {
        public void onOrdersDetailsDataCallBack(OrderDetailsModel orderDetails);
    }

    private MyAccountFragment mAccountFragment;

    public OrderDetailsAsyncTask(MyAccountFragment accountFragment) {
        mAccountFragment = accountFragment;
    }
    @Override
    protected OrderDetailsModel doInBackground(String... strings) {
        GetOrderDetails orderDetails = new GetOrderDetails();
        OrderDetailsModel orderDetailsModel = orderDetails.getOrdersDetailsData(strings[0]);
        return orderDetailsModel;
    }

    @Override
    protected void onPostExecute(OrderDetailsModel orderDetailsModel) {
        super.onPostExecute(orderDetailsModel);
        mAccountFragment.onOrdersDetailsDataCallBack(orderDetailsModel);
    }
}
