package reimu.edge.object.reimu.network;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.net.URL;

import reimu.edge.object.reimu.utils.HttpUtils;

/**
 * Created by owner on 3/13/18.
 */

public class GetOrdersAPI {

    private static final String ORDERS_API =
            "http://crs113.objectedge.com:7003/public/v1/orders?userId=400000";
    private static final String TAG = GetOrdersAPI.class.getSimpleName();
    private String orderDetails;

    public JSONArray getOrdersData() {
        JSONArray jsonArray = null;
        try {
            orderDetails = HttpUtils.processRequestandGetResponse(new URL(ORDERS_API));
            jsonArray = new JSONArray(orderDetails);

        } catch (IOException ioException) {
            Log.e(TAG, ioException.getMessage());
        } catch (JSONException jsonException) {
            Log.e(TAG, jsonException.getMessage());

        }

        return jsonArray;
    }
}
